package com.talipov;

import org.springframework.stereotype.Component;

/**
 * Created by marsel on 23.03.17.
 */
@Component
public class VoluteCoursesImpl implements VoluteCourses {
    public Float convert(Float sum) {
        return sum * 63.5f;
    }
}
